package it.unimi.di.sweng.lab03;

import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;
import java.util.function.IntPredicate;

public class IntegerList {
	private IntegerNode head = null;
	private IntegerNode tail = null;
	private IntegerNode index = null;
	
	public IntegerList(){
		this("");
	}
	
	public IntegerList(String text){
		Scanner numbers = new Scanner(text);
		while(numbers.hasNext())
			addLast(Integer.valueOf(numbers.next()));
		numbers.close();
	}
	
	public IntegerList(InputStreamReader file) throws IOException {
		int ch = file.read();
		String text = "";
		while (ch > 0){
			text += (char)ch;
			ch = file.read();
		}
		Scanner numbers = new Scanner(text);
		while(numbers.hasNext())
			addLast(Integer.valueOf(numbers.next()));
		numbers.close();
	}

	public String toString(){
		String text = "[";
		IntegerNode current = head;
		int i = 0;
		while(current != null){
			if(i++ > 0)
				text += " ";
			text += current.getValue();
			current = current.getNext();
		}
		return text+"]";
	}
	
	public void addLast(int i){
		if(head == null){
			head = tail = new IntegerNode(i);
			index = head;
		}
		else{
			tail.setNext(new IntegerNode(i));
			tail = tail.getNext();
		}
			
	}

	public void addFirst(int i) {
		IntegerNode current = new IntegerNode(i);
		current.setNext(head);
		head = current;
	}

	public boolean removeFirst() {
		if(head == null)
			return false;
		head = head.getNext();
		return true;
	}

	public boolean removeLast() {
		if(tail == null)
			return false;
		if(tail == head){
			head = tail = null;
			return true;
		} 
		IntegerNode current = head;
		while(current.getNext() != tail)
			current = current.getNext();
		tail = current;
		tail.setNext(null);
		return true;
	}

	public boolean remove(int value) {
		IntegerNode current = head;
		IntegerNode previous = head;
		while(current.getValue() != value){
			previous = current;
			current = current.getNext();
			if(current == tail && current.getValue() != value)
				return false;
		}
		if(current == head)
			removeFirst();
		else if(current == tail)
			removeLast();
		else
			previous.setNext(current.getNext());
		return true;
	}

	public boolean removeAll(int i) {
		int numRemoved = 0;
		while(remove(i))
			numRemoved++;
		if(numRemoved == 0)
			return false;
		return true;
	}

	public double mean() {
		double totNumbers = 0;
		double partial = 0;
		IntegerNode current = head;
		while(current.getNext() != null){
			partial += current.getValue();
			totNumbers++;
			current = current.getNext();
		}
		partial += tail.getValue();
		totNumbers++;
		return partial/totNumbers;
	}

    public double stdDev(){
    	if(head == null || head.getNext() == null)
			return 0;
        double mean = mean();												//calculate mean of array.
        double partial = 0;
        double totNumbers = 0;
        IntegerNode current = head;
        while(current.getNext() != null){									//loop through values
			partial += (current.getValue()-mean)*(current.getValue()-mean); //partial = partial+(indexed value - mean)^2    
			totNumbers++;
			current = current.getNext();
		}
        partial += (tail.getValue()-mean)*(tail.getValue()-mean);
        partial = (int)(Math.sqrt(partial/totNumbers)*1000);
        return (double)partial/1000;										//divide the sum by the array length-1 and square root it
    }

	public Integer prev() {
		if(index == head)
			return head.getValue();
		int value = index.getValue();
		IntegerNode current = head;
		while(current.getNext() != index)
			current = current.getNext();
		index = current;
		return value;
	}

	public Integer next() {
		if(index == tail)
			return tail.getValue();
		int value = index.getValue();
		index = index.getNext();
		return value;
	}
	
}
