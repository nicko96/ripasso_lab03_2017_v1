package it.unimi.di.sweng.lab03;

public class IntegerNode {
	private Integer value = null;
	private IntegerNode next = null;
	
	public IntegerNode(int i) {
		value = i;
	}

	public IntegerNode getNext() {
		return next;
	}

	public void setNext(IntegerNode current) {
		next = current;
		
	}

	public Integer getValue() {
		return value;
	}
	
}
