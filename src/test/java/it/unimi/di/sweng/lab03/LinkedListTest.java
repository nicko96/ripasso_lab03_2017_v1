package it.unimi.di.sweng.lab03;

import static org.assertj.core.api.Assertions.*;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.Timeout;

public class LinkedListTest {

	@Rule
    public Timeout globalTimeout = Timeout.seconds(2);

	private IntegerList list;
	
	@Test
	public void noParamConstructor() {
		list = new IntegerList();
		assertThat(list.toString()).isEqualTo("[]");
	}
	
	@Test
	public void addLastTest() {
		list = new IntegerList();
		list.addLast(1);
		assertThat(list.toString()).isEqualTo("[1]");
		list.addLast(3);
		assertThat(list.toString()).isEqualTo("[1 3]");
	}
	
	@Test
	public void stringConstructor() {
		list = new IntegerList("");
		assertThat(list.toString()).isEqualTo("[]");
		list = new IntegerList("1");
		assertThat(list.toString()).isEqualTo("[1]");
		list = new IntegerList("1 2 3");
		assertThat(list.toString()).isEqualTo("[1 2 3]");
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void robustnessConstructor() {
		list = new IntegerList("1 2 aaa");
	}
	
	@Test
	public void addFirstTest() {
		list = new IntegerList();
		list.addFirst(1);
		assertThat(list.toString()).isEqualTo("[1]");
		list.addFirst(3);
		assertThat(list.toString()).isEqualTo("[3 1]");
	}
	
	@Test
	public void removeFirstTest(){
		list = new IntegerList("1 2");
		list.removeFirst();
		assertThat(list.toString()).isEqualTo("[2]");
		list.removeFirst();
		assertThat(list.toString()).isEqualTo("[]");
		assertThat(!(list.removeFirst()));
	}
	
	@Test
	public void removeLastTest() {
		list = new IntegerList("7 10");
		list.removeLast();
		assertThat(list.toString()).isEqualTo("[7]");
		list.removeLast();
		assertThat(list.toString()).isEqualTo("[]");
		assertThat(!(list.removeLast()));
	}
	
	@Test
	public void removeTest() {
		list = new IntegerList("1 2 3 4 3 5");
		list.remove(2);
		assertThat(list.toString()).isEqualTo("[1 3 4 3 5]");
		list.remove(3);
		assertThat(list.toString()).isEqualTo("[1 4 3 5]");
		assertThat(!(list.remove(6)));
	}
	
	@Test
	public void removeAllTest() {
		list = new IntegerList("1 2 3 4 3 5");
		list.removeAll(3);
		assertThat(list.toString()).isEqualTo("[1 2 4 5]");
		assertThat(!(list.removeAll(6)));
	}
	
	@Test
	public void meanTest() {
		list = new IntegerList("1 2");
		assertThat(list.mean()).isEqualTo(1.5);
		list = new IntegerList("160 591 114 229 230 270 128 1657 624 1503");
		assertThat(list.mean()).isEqualTo(550.6);
	}
	
	@Test
	public void stdDevTest() {
		list = new IntegerList();
		assertThat(list.stdDev()).isEqualTo(0);
		list = new IntegerList("1");
		assertThat(list.stdDev()).isEqualTo(0);
		list = new IntegerList("160 591 114 229 230 270 128 1657 624 1503");
		assertThat(list.stdDev()).isEqualTo(572.026);
	}
	
	@Test
	public void nextPrevTest() {
		list = new IntegerList("1 2 3");
		assertThat(list.prev()).isEqualTo(1);
		assertThat(list.next()).isEqualTo(1);
		assertThat(list.prev()).isEqualTo(2);
		assertThat(list.next()).isEqualTo(1);
		assertThat(list.next()).isEqualTo(2);
		assertThat(list.next()).isEqualTo(3);
	}
	
	@Test
	public void fileConstructor() throws UnsupportedEncodingException, IOException {
		FileInputStream stream = new FileInputStream("src/test/resources/input.txt");
		list = new IntegerList(new InputStreamReader(stream, "UTF-8"));
		assertThat(list.toString()).isEqualTo("[7 10 8 6 7 4 10]");
	}
}